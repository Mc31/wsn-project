# WSN Project

Implementation of sensor data gathering using IPv6 protocol (6Lowpan).
  
Three Sensors Water sensor, BME280 – Humiditiy, Temperature and Barometric Pressure, and PIR Motion sensor.
Each are connected to the Firefly ZOL BO001-A2. Using 
Internet Protocol v6, IPv6
User Datagram Protocol
Display sensor data to a Terminal
Save data into text file using third party Serial Monitor Application (gtkTerm)


The project primarily aims to provide a system of sensor motes that transmits data to a server mote with the use of Contiki. 
The project also aims to achieve the following objectives:
	To be able to transmit sensors’ measured data
	To be able to receive and present the sensors’ 	measured data 
	To be able to store the measured data
	
	
To Install gtkTerm app
First, Open Terminal then type this to
$ cd ~

Second is to update using this command
$ sudo apt-get update -y

Third, Install
$ sudo apt-get install -y gtkterm

Then Run the app
$ sudo gtkterm
